export const createProfileList = [
    {
        image: require('../assets/images/user.png'),
        label: 'Self'
    },
    {
        image: require('../assets/images/collaboration.png'),
        label: 'Relative'
    },
    {
        image: require('../assets/images/boy-smiling.png'),
        label: 'Son'
    },
    {
        image: require('../assets/images/smiling-girl.png'),
        label: 'Daughter'
    },
    {
        image: require('../assets/images/friendship.png'),
        label: 'Brother'
    },
    {
        image: require('../assets/images/friendship (1).png'),
        label: 'Sister'
    },
    {
        image: require('../assets/images/high-five.png'),
        label: 'Friend'
    }
]

export const dataSource = [1,2,3,4,5,6,7,8,9,10,11,12] 


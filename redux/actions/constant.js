export const API_URL = 'https://thinkmatters.softuvo.xyz/api';

// export const API_CHAT_URL = 'http://localhost:3003';

export const API_CHAT_URL = 'http://209.97.142.219:3003';

// fontSizes
export const fontSize15 = 15;
export const fontSize19 = 19;
export const fontSize12 = 12;
export const fontSize16 = 16;
export const fontSize14 = 14;
export const fontSize10 = 10;


// colors
export const primaryColor = '#f05055';
export const whiteColor = '#fff';
export const blackColor = '#000';
export const greyColor = '#515250';


// font-family

export const fontFamily = 'verdana';

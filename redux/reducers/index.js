import { combineReducers }  from 'redux'
import userData from './userData/index' 

export default combineReducers({
    userData
})